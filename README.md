# Rasp API

[![pipeline status](https://gitlab.com/klara-org/rasp-api/badges/master/pipeline.svg)](https://gitlab.com/klara-org/rasp-api/commits/master)

[![coverage report](https://gitlab.com/klara-org/rasp-api/badges/master/coverage.svg)](https://gitlab.com/klara-org/rasp-api/commits/master)

# Getting Started
Rasp API is a Sinatra-based application that runs inside Klara Equipments. It is responsible for managing samples collection, saving and sending them to Klara API.

# Useful Commands
## Running the server
```bash
ruby app.rb
```

You can specify other configurations too (like the port and host).
```bash
ruby app.rb -o 0.0.0.0 -p 8000
```

## Console
```bash
bundle exec irb -I. -r app.rb
```

If you like, there's some rake tasks to make it easier for you.
```bash
rake clean            # Cleans all samples
rake clean:new        # Cleans new samples
rake clean:saved      # Cleans saved samples
rake console          # Opens a ruby console
rake server           # Runs the server
rake test             # Runs all tests
rake test:unit        # Runs unit tests
rake test:functional  # Runs functional tests
```

# Structure
* **app.rb:** Main file. Where the routes/controllers are located on.
* **rasp.rb:** Configuration file, environment variables and constants.
* **data/**
  * **new_samples/:** Freshly collected samples.
  * **saved_samples/:** Samples saved offline to be posted in the future on Klara API.
* **helpers/:** Some useful helper modules.
* **views/:** HTML Templates.
