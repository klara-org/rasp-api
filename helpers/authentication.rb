# frozen_string_literal: true

module Authentication
  require "openssl"
  require_relative "test_utils"

  def encrypt_password(setup_data)
    encrypter = OpenSSL::Cipher.new "AES-128-CBC"
    encrypter.encrypt
    encrypter.pkcs5_keyivgen setup_data[:key], setup_data[:salt]

    password_digest = encrypter.update generate_password(setup_data)
    password_digest << encrypter.final

    Base64.encode64(password_digest).encode("utf-8")
  end

  def generate_password(setup_data)
    password = last_chars(8, setup_data[:setup_date]) + last_chars(8, setup_data[:serial_number])
  end
end
