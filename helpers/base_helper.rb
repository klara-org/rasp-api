# frozen_string_literal: true

module Sinatra
  require_relative "api_connection"
  require_relative "file_manager"
  require_relative "test_utils"
  require_relative "authentication"
  require_relative "qr_code_handler"
  require_relative "datasheet"

  helpers APIConnection, FileManager, TestUtils, Authentication, QRCodeHandler, Datasheet
end
