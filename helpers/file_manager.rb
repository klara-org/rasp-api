# frozen_string_literal: true

module FileManager
  require "httparty"
  require "rubygems"
  require "zip"

  def file_exists?(path)
    File.file?(path)
  end

  def delete_file(path)
    File.delete(path) if file_exists?(path)
  end

  def read_file(path)
    file_content = ""
    file = File.open(path, "r")
    file.each_line do |line|
      file_content += line
    end
    file.close

    file_content
  end

  def format_input(file_content)
    input = []

    file_content.split("\n").each do |data|
      key, value = data.split(": ")
      input.push(key.to_sym => value)
    end

    formated_input = input.inject(:merge)
    formated_input
  end

  def format_output(sample)
    # Removes unnecessary chars ('-' ':' and ' ') to create filename
    collection_date = sample[:collection_date].gsub(/[-: ]/, "")
    { name: collection_date + ".json", content: sample.to_json }
  end

  # TODO: tornar genérico
  def save_sample_locally(sample)
    file = File.new(Rasp::SAVED_SAMPLES_PATH + sample[:name], "w")
    file.write(sample[:content])
    file.close
  end

  def download_samples
    files = Dir.glob(Rasp::SAVED_SAMPLES_PATH + "*.json")

    delete_file(Rasp::ZIP_FILENAME)

    Zip::File.open(Rasp::ZIP_FILENAME, Zip::File::CREATE) do |zipfile|
      files.each do |filename_with_path|
        filename = File.basename(filename_with_path)
        zipfile.add(filename, filename_with_path)
      end
    end
  end
end
