# frozen_string_literal: true

module TestUtils
  def debug(variable, char = "*")
    p char * 80
    p variable
    p char * 80
  end
end
