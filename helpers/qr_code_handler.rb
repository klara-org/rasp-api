# frozen_string_literal: true

module QRCodeHandler
  require "qrio"

  def valid_setup_code?(setup_code)
    setup_code.length == Rasp::SETUP_CODE_LENGTH
  end

  def format_setup_code(qr_code)
    setup_data = {
      serial_number: get_string_chars(0, 32, qr_code),
      key: get_string_chars(32, 32, qr_code),
      salt: get_string_chars(64, 8, qr_code),
      setup_date: setup_date
    }
  end

  def get_string_chars(start, size, str)
    str.chars[start..(start + size - 1)].join
  end

  def last_chars(size, str)
    str.chars.last(size).join
  end
end
