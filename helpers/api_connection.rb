# frozen_string_literal: true

module APIConnection
  require "net/ping"
  require "json"

  MAX_RETRIES = 5
  SLEEP_TIME = 1

  # TODO: Fazer método para verificar primeira configuração do equipamento
  def has_internet_connection?
    has_connection = false

    MAX_RETRIES.times do |retry_number|
      puts "[#{MAX_RETRIES - retry_number}] Checking internet connection..." unless ENV["RACK_ENV"] == "test"
      has_connection = Net::Ping::External.new("8.8.8.8").ping?
      break if has_connection

      sleep SLEEP_TIME
    end

    unless ENV["RACK_ENV"] == "test"
      puts has_connection ? "[LOG] Hooray! There is internet connection." : "[ERROR] Oh-oh! There is no internet connection."
    end

    has_connection
  end

  def has_saved_samples?
    !Dir.empty?(Rasp::SAVED_SAMPLES_PATH)
  end

  def delete_sent_samples(responses, file_paths)
    responses.shift # skip the first response, because it is from the newly created file (that was not locally saved)
    responses.each_with_index do |response, index|
      File.delete(file_paths[index]) if response == 201
    end
  end

  def new_sample
    file = read_file(Rasp::NEW_SAMPLES_PATH)
    sample = format_input(file)
    sample[:serial_number] = Rasp.datasheet[:serial_number]
    if has_internet_connection?
      samples = [sample.to_json]
      saved_files = Dir[Rasp::SAVED_SAMPLES_PATH + "*.json"]

      if saved_files.empty?
        response = send_samples(samples)
        puts "[LOG] Sample have been sent.\n"
        halt response.first, "Sample have been sent."
      else
        saved_files.each do |file_path|
          samples << File.read(file_path)
        end
        responses = send_samples(samples)
        delete_sent_samples(responses, saved_files)

        if responses.all? { |response| response == 201 }
          puts "[LOG] Samples have been sent.\n"
          halt 201, "Samples have been sent."
        else
          puts "[LOG] Some samples couldn't be saved.\n"
          halt 207, "Some samples couldn't be saved."
        end
      end
    else
      formated_output = format_output(sample)
      save_sample_locally(formated_output)
      puts "[LOG] Samples saved locally.\n"
      halt 200, "Samples saved locally."
    end
  end

  def authenticate_equipment
    data = Rasp.datasheet
    credentials = {
      equipment: {
        serial_number: data[:serial_number],
        setup_code: data[:setup_code],
        password: data[:password]
      }
    }
    response = HTTParty.post(Rasp::API_AUTHENTICATION_ENDPOINT, headers: { "Content-Type" => "application/json" }, body: credentials.to_json)
  end

  def send_samples(samples)
    responses = []
    authentication = JSON.parse(authenticate_equipment.body)
    samples.each do |sample|
      response = HTTParty.post(Rasp::POST_SAMPLE_API_URL, headers: { "Content-Type" => "application/json", "Authorization" => authentication["token"] }, body: sample)
      responses << response.code
    end
    responses
  end

  def send_datasheet(datasheet)
    data = JSON.parse(datasheet)
    body = {
      equipment: {
        serial_number: data["serial_number"],
        setup_code: data["setup_code"],
        password: data["password"],
        setup_date: data["setup_date"]
      }
    }
    response = HTTParty.post(Rasp::POST_DATASHEET_API_URL, headers: { "Content-Type" => "application/json" }, body: body.to_json)
  end
end
