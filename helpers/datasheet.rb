# frozen_string_literal: true

module Datasheet
  def save_datasheet(file_content)
    file = File.new(Rasp::DATASHEET_FILENAME, "w")
    file.write(file_content)
    file.close
    send_datasheet(file_content)
  end

  def build_datasheet(qr_code)
    setup_data = format_setup_code(qr_code)

    datasheet = {
      setup_code: qr_code,
      serial_number: setup_data[:serial_number],
      password: encrypt_password(setup_data),
      setup_date: setup_data[:setup_date]
      # TODO
      # geolocation: '90, -12',
      # owner: '',
      # TODO
      # wifi_credentials: ''
    }

    save_datasheet(datasheet.to_json)
  end

  def setup_date
    Time.now.strftime "%Y%m%d%H%M%S%N"
  end
end
