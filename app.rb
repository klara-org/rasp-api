# frozen_string_literal: true

require "sinatra"
require_relative "rasp"
require_relative "helpers/base_helper"

get "/" do
  erb :root, layout: :base
end

get "/equipment" do
  check_datasheet
  erb :equipment, layout: :base
end

get "/register/:setup_code" do |setup_code|
  if Rasp.datasheet.nil?
    if valid_setup_code?(setup_code)
      build_datasheet(setup_code)
      halt 201, "Datasheet created"
    else
      halt 403, "Invalid QR Code"
    end
  else
    halt 304, "Already registered"
  end
end

get "/register" do
  erb :register, layout: :base
end

get "/download/datasheet" do
  check_datasheet
  send_file Rasp::DATASHEET_FILENAME, disposition: "attachment"
end

get "/download/samples" do
  download_samples
  send_file Rasp::ZIP_FILENAME
end

# TODO
get "/wifi_setup" do
  erb :wifi_setup, layout: :base
end

get "/new_sample" do
  new_sample
end

#### Before actions ####
def check_datasheet
  redirect "/register" if Rasp.datasheet.nil?
end
