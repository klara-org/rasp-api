FROM ruby:2.5.1-alpine3.7

RUN apk update && apk upgrade \
  && apk add ca-certificates \
  && rm -rf /var/cache/apk/* \
  && apk add --no-cache \
    libffi openssl libffi-dev \
    build-base openssl-dev

COPY . /rasp-api
WORKDIR /rasp-api
RUN bundle install
ENTRYPOINT [ "rake", "server" ]
