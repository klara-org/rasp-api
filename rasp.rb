# frozen_string_literal: true

module Rasp
  # Constants
  NEW_SAMPLES_PATH = "data/new_samples/sample.txt"
  SAVED_SAMPLES_PATH = "data/saved_samples/"
  ZIP_FILENAME = "data/samples.zip"
  DATASHEET_FILENAME = "data/datasheet/datasheet.json"
  SETUP_CODE_LENGTH = 72 # Bytes
  # TODO: Change these URLs when Klara API is deployed
  POST_SAMPLE_API_URL = "http://localhost:3000/samples"
  POST_DATASHEET_API_URL = "http://localhost:3000/equipments"
  API_AUTHENTICATION_ENDPOINT = "http://localhost:3000/authenticate_equipment"

  def self.datasheet
    datasheet_json = File.read(DATASHEET_FILENAME)
    datasheet_hash = JSON.parse(datasheet_json, symbolize_names: true)
    datasheet_hash
  rescue Errno::ENOENT
    nil
  end
end
