# frozen_string_literal: true

require "rake/testtask"

desc "Runs the server"
task :server do
  system("bundle exec rerun 'ruby app.rb -o 0.0.0.0 -p 8000'")
end
task s: :server

desc "Opens a ruby console"
task :console do
  system("bundle exec irb -I. -r app.rb")
end
task c: :console

desc "Cleans all samples"
task :clean do
  ["clean:new", "clean:saved"]
end

desc "Outputs TODOS and FIXME comments"
task :notes do
  cmd = ["#TODO", "# TODO", "#FIXME", "# FIXME"].map do |comment|
    "grep --exclude=Rakefile -rni '#{comment}' ."
  end .join(" && ")
  system(cmd)
end

namespace :clean do
  desc "Cleans new samples"
  task :new do
    system("rm data/new_samples/*.txt")
  end

  desc "Cleans saved samples"
  task :saved do
    system("rm data/saved_samples/*.json")
  end
end

namespace :test do
  Rake::TestTask.new do |t|
    t.name = :unit
    t.description = "Runs unit tests"
    t.pattern = "test/unit/*_test.rb"
    t.warning = false
  end

  Rake::TestTask.new do |t|
    t.name = :functional
    t.description = "Runs functional tests"
    t.pattern = "test/functional/*_test.rb"
    t.warning = false
  end
end

# Task to run all tests
Rake::TestTask.new do |t|
  t.pattern = "test/**/*_test.rb"
  t.warning = false
end
