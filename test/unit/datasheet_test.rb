# frozen_string_literal: true

require_relative "../test_helper"

class DatasheetTest < MainAppTest
  include Datasheet
  include APIConnection
  include QRCodeHandler
  include Authentication

  def setup
    @datasheet = {
      "setup_code": "74943a31031e58db3a72f94025203706b0af04f59f8d8d0c196f33b1555092fcdeba016f",
      "serial_number": "74943a31031e58db3a72f94025203706",
      "password_digest": "2VtMCsQGfeCo47VQCAESE+Nrrke9Ku2XjZT2UD2U1oQ=\n",
      "setup_date": "20180925092654714433946"
    }.to_json
  end

  def test_save_datasheet
    Rasp.stub_const(:DATASHEET_FILENAME, "data/tmp-datasheet.json") do
      expects(:send_datasheet)

      save_datasheet(@datasheet)

      assert File.file?(Rasp::DATASHEET_FILENAME)
      File.delete Rasp::DATASHEET_FILENAME
    end
  end

  def test_build_datasheet
    qr_code = "1" * 32 + "2" * 32 + "3" * 8
    setup_data = { serial_number: "1234", setup_date: "01-01-2001" }

    stubs(:encrypt_password)
    stubs(:format_setup_code).returns(setup_data)
    expects(:save_datasheet)

    build_datasheet(qr_code)
  end

  def test_setup_date
    test_time = Time.parse("2018-11-02 18:00:00 UTC")
    Time.stubs(:now).returns(test_time)
    assert_equal "20181102180000000000000", setup_date
  end
end
