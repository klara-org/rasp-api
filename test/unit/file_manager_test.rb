# frozen_string_literal: true

require_relative "../test_helper"

class FileManagerTest < MainAppTest
  include FileManager

  def setup
    @valid_path = "test/test.tmp"
    @invalid_path = "this/is/an/invalid/file/path"
    delete(@valid_path)
  end
  attr_reader :valid_path, :invalid_path

  def test_valid_file_existence
    assert file_exists?("app.rb")
  end

  def test_invalid_file_existence
    refute file_exists?("invalid/file/path")
  end

  def test_valid_file_deletion
    File.open(valid_path, "w")
    delete_file(valid_path)

    refute file_exists?(valid_path)
  end

  def test_invalid_file_deletion
    File.expects(:delete).never
    delete_file(invalid_path)
  end

  def test_file_reading
    content = "Ruby is awesome!"
    f = File.open(valid_path, "w")
    f.write(content)
    f.close

    assert_equal content, read_file(valid_path)
    delete_file(valid_path)
  end

  def test_sample_file_reading
    file_content = <<~HEREDOC
      temperature: 20
      collection_date: 2018-09-27 19:41:38
    HEREDOC
    formated_input = format_input(file_content)

    assert_equal "20", formated_input[:temperature]
    assert_equal "2018-09-27 19:41:38", formated_input[:collection_date]
  end

  def test_sample_formating
    sample = {
      temperature: "20",
      collection_date: "2018-09-27 19:41:38"
    }
    formated_sample = format_output(sample)

    assert_equal "20180927194138.json", formated_sample[:name]
    assert_equal sample.to_json, formated_sample[:content]
  end

  def test_sample_formating
    sample = {
      temperature: "20",
      collection_date: "2018-09-27 19:41:38"
    }
    formated_sample = format_output(sample)

    assert_equal "20180927194138.json", formated_sample[:name]
    assert_equal sample.to_json, formated_sample[:content]
  end

  def test_sample_local_saving
    sample = {
      name: "20180927194138.json",
      content: "{\"temperature\":\"20\",\"collection_date\":\"2018-09-27 19:41:38\"}"
    }

    save_sample_locally(sample)

    saved_sample = Rasp::SAVED_SAMPLES_PATH + sample[:name]
    assert file_exists?(saved_sample)
    assert_equal sample[:content], File.read(saved_sample)
  end

  def test_download_saved_samples
    another_valid_path = "test/test_test.tmp"
    File.open(valid_path, "w")
    File.open(another_valid_path, "w")
    File.open(Rasp::ZIP_FILENAME, "w")
    expects(:delete_file)

    download_samples

    assert file_exists?(Rasp::ZIP_FILENAME)

    File.delete(Rasp::ZIP_FILENAME)
    File.delete(valid_path)
    File.delete(another_valid_path)
  end
end
