# frozen_string_literal: true

require_relative "../test_helper"

class AuthenticationTest < MainAppTest
  include Authentication
  include QRCodeHandler
  include Datasheet

  def setup
    qr_code = "1" * 32 + "2" * 32 + "3" * 8
    stubs(:setup_date).returns("20181102180000000000000")
    @setup_data = format_setup_code(qr_code)
  end

  def test_password_encryption
    assert_equal "ytFpEIDr7k8MH4C94Klifi7o2ahjKE6sbQrCrcOc2xM=\n", encrypt_password(@setup_data)
  end

  def test_password_generation
    assert_equal "0000000011111111", generate_password(@setup_data)
  end
end
