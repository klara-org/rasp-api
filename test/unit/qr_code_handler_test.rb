# frozen_string_literal: true

require_relative "../test_helper"

class QRCodeHandlerTest < MainAppTest
  include QRCodeHandler
  include Datasheet

  def test_valid_setup_code
    setup_code = "74943a31031e58db3a72f94025203706b0af04f59f8d8d0c196f33b1555092fcdeba016f"
    assert valid_setup_code?(setup_code)
  end

  def test_invalid_setup_code
    refute valid_setup_code?("Inv4l1d-SetupC0de")
  end

  def test_format_setup_code
    setup_code = "1" * 32 + "2" * 32 + "3" * 8
    setup_data = format_setup_code(setup_code)

    assert_equal "1" * 32, setup_data[:serial_number]
    assert_equal "2" * 32, setup_data[:key]
    assert_equal "3" * 8, setup_data[:salt]
    assert_equal [:serial_number, :key, :salt, :setup_date].sort, setup_data.keys.sort
  end

  def test_get_string_chars
    string = "my-awesome-string"

    assert_equal "my", get_string_chars(0, 2, string)
    assert_equal "awesome", get_string_chars(3, 7, string)
  end

  def test_last_chars
    string = "my-awesome-string"

    assert_equal "string", last_chars(6, string)
    assert_equal "awesome-string", last_chars(14, string)
    assert_equal "", last_chars(0, string)
  end
end
