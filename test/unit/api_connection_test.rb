# frozen_string_literal: true

require_relative "../test_helper"

class APIConnectionTest < MainAppTest
  include APIConnection
  include FileManager

  def setup
    APIConnection.send(:remove_const, :SLEEP_TIME)
    APIConnection.const_set(:SLEEP_TIME, 0.1)
  end

  def test_internet_connection
    dummy = mock()
    Net::Ping::External.stubs(:new).returns(dummy)
    dummy.stubs(:ping?).returns(true)
    assert has_internet_connection?

    dummy.stubs(:ping?).returns(false)
    refute has_internet_connection?
  end

  def test_should_output_logs_outside_test_env
    ENV["RACK_ENV"] = "another_env"
    output, errors = capture_io do
      dummy = mock()
      Net::Ping::External.stubs(:new).returns(dummy)
      dummy.stubs(:ping?).returns(false)
      refute has_internet_connection?
    end

    assert_match /Checking internet connection.../, output
    ENV["RACK_ENV"] = "test"
  end

  def test_saved_samples_verification
    Dir.stubs(:empty?).returns(false)
    assert has_saved_samples?

    Dir.stubs(:empty?).returns(true)
    refute has_saved_samples?
  end

  def test_sent_samples_deletion
    responses = [201, 201]
    file_path = ["test/f1.json"]
    File.open(file_path[0], "w")

    delete_sent_samples(responses, file_path)

    refute file_exists?(file_path[0])
  end

  def test_should_not_delete_sample_if_response_is_not_201
    responses = [201, 500]
    file_path = ["test/f1.json"]
    File.open(file_path[0], "w")

    delete_sent_samples(responses, file_path)

    assert file_exists?(file_path[0])
    delete_file(file_path[0])
  end

  def test_should_send_current_sample
    output, errors = capture_io do
      Rasp.stub_const(:SAVED_SAMPLES_PATH, "test/") do
        app.any_instance.stubs(:read_file)
        app.any_instance.stubs(:format_input).returns({})
        serial_number = "1" * 32
        Rasp.stubs(:datasheet).returns(serial_number: serial_number)
        app.any_instance.stubs(:has_internet_connection?).returns(true)
        sample_filename = "sample.json"
        Dir.stubs(:[]).returns([])
        app.any_instance.stubs(:send_samples).returns([201])

        get "/new_sample"

        assert last_response.created?
      end
    end
    assert_match /Sample have been sent./, output
  end

  def test_should_send_current_sample_and_saved_ones
    output, errors = capture_io do
      Rasp.stub_const(:SAVED_SAMPLES_PATH, "test/") do
        app.any_instance.stubs(:read_file)
        app.any_instance.stubs(:format_input).returns({})
        serial_number = "1" * 32
        Rasp.stubs(:datasheet).returns(serial_number: serial_number)
        app.any_instance.stubs(:has_internet_connection?).returns(true)
        sample_filename = "sample.json"
        Dir.stubs(:[]).returns(["not_empty"])
        File.stubs(:read)
        app.any_instance.stubs(:send_samples).returns([201, 201])
        app.any_instance.stubs(:delete_sent_samples)

        get "/new_sample"

        assert last_response.created?
      end
    end
    assert_match /Samples have been sent./, output
  end

  def test_should_alert_about_errors
    output, errors = capture_io do
      Rasp.stub_const(:SAVED_SAMPLES_PATH, "test/") do
        app.any_instance.stubs(:read_file)
        app.any_instance.stubs(:format_input).returns({})
        serial_number = "1" * 32
        Rasp.stubs(:datasheet).returns(serial_number: serial_number)
        app.any_instance.stubs(:has_internet_connection?).returns(true)
        sample_filename = "sample.json"
        Dir.stubs(:[]).returns(["not_empty"])
        File.stubs(:read)
        app.any_instance.stubs(:send_samples).returns([201, 500])
        app.any_instance.stubs(:delete_sent_samples)

        get "/new_sample"

        assert_response :multi_status, last_response
      end
    end
    assert_match /Some samples couldn't be saved./, output
  end

  def test_should_locally_save_new_sample_if_there_is_no_connection
    output, errors = capture_io do
      Rasp.stub_const(:SAVED_SAMPLES_PATH, "test/") do
        app.any_instance.stubs(:read_file)
        app.any_instance.stubs(:format_input).returns({})
        serial_number = "1" * 32
        Rasp.stubs(:datasheet).returns(serial_number: serial_number)
        app.any_instance.stubs(:has_internet_connection?).returns(false)
        sample_filename = "sample.json"
        app.any_instance.stubs(:format_output).returns(name: sample_filename, content: "{ \"turbidity\": 20 }")
        get "/new_sample"

        assert last_response.ok?
        created_sample = Rasp::SAVED_SAMPLES_PATH + sample_filename
        assert file_exists?(created_sample)
        delete_file created_sample
      end
    end
    assert_match /Samples saved locally./, output
  end

  def test_equipment_authentication
    datasheet = mock()
    datasheet.stubs(:[])
    Rasp.stubs(:datasheet).returns(datasheet)
    HTTParty.expects(:post).returns(200)

    response = authenticate_equipment
    assert_response :ok, response
  end

  def test_samples_send
    sample_1 = <<~HEREDOC
      {
        "temperature":"20"
      }
    HEREDOC
    sample_2 = <<~HEREDOC
      {
        "temperature":"20"
      }
    HEREDOC
    samples = [sample_1, sample_2]
    authentication = authentication_response = mock()

    authentication.stubs(:[])
    authentication_response.stubs(:body).returns("body")

    self.stubs(:authenticate_equipment).returns(authentication_response)
    JSON.stubs(:parse).returns(authentication)

    response_1, response_2 = mock, mock
    response_1.stubs(:code).returns(200)
    response_2.stubs(:code).returns(500)

    HTTParty.expects(:post).twice.returns(response_1, response_2)

    responses = send_samples(samples)

    assert_equal [200, 500], responses
  end

  def test_datasheet_send
    datasheet_json = <<~HEREDOC
      {
        "serial_number": "1234",
        "setup_code": "1234",
        "password": "1234",
        "setup_date":"20181103"
      }
    HEREDOC
    HTTParty.expects(:post).returns(200)

    response = send_datasheet(datasheet_json)
    assert_response :ok, response
  end
end
