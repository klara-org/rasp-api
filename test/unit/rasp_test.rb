# frozen_string_literal: true

require_relative "../test_helper"

class RaspTest < MainAppTest
  include Rasp

  def test_datasheet
    datasheet_json = <<~HEREDOC
      {
        "setup_code": "1234",
        "setup_date":"20181103"
      }
    HEREDOC
    File.stubs(:read).returns(datasheet_json)
    datasheet_hash = Rasp.datasheet

    assert_equal datasheet_hash[:setup_code], "1234"
    assert_equal datasheet_hash[:setup_date], "20181103"
  end

  def test_should_return_nil_if_there_is_no_datasheet
    File.stubs(:read).raises(Errno::ENOENT)

    assert_nil Rasp.datasheet
  end
end
