# frozen_string_literal: true

require_relative "../test_helper"

class FunctionalTest < MainAppTest
  include FileManager

  def test_homepage
    Rasp.stubs(:datasheet).returns(true)
    get "/"
    assert last_response.ok?

    assert last_response.body.include?("<p>Equipment Info</p>")
    assert last_response.body.include?("<p>Download Datasheet</p>")
    assert last_response.body.include?("<p>Download Samples</p>")
    assert last_response.body.include?("<p>Wi-Fi Setup</p>")
    refute last_response.body.include?("<p>Register Equipment</p>")
  end

  def test_homepage_should_render_register_if_there_is_no_datasheet
    Rasp.stubs(:datasheet).returns(false)
    get "/"
    assert last_response.ok?

    assert last_response.body.include?("<p>Register Equipment</p>")
    refute last_response.body.include?("<p>Equipment Info</p>")
    refute last_response.body.include?("<p>Download Datasheet</p>")
    refute last_response.body.include?("<p>Download Samples</p>")
    refute last_response.body.include?("<p>Wi-Fi Setup</p>")
  end

  def test_should_show_equipment
    datasheet = mock()
    datasheet.stubs(:[])
    Rasp.stubs(:datasheet).returns(datasheet)
    get "/equipment"

    assert(last_response.ok?)
    assert last_response.body.include?("<h1>Equipment Info</h1>")
    assert last_response.body.include?("<b>Setup Code: </b>")
    assert last_response.body.include?("<p>Main Menu</p>")
  end

  def test_should_redirect_to_register_if_there_is_no_datasheet
    Rasp.stubs(:datasheet).returns(nil)
    get "/equipment"

    assert_redirected_to("register", last_response)
    assert(last_response.redirect?)
  end

  def test_should_get_register_page
    get "/register"

    assert(last_response.ok?)
    assert last_response.body.include?("<h1>Register Your Equipment</h1>")
    assert last_response.body.include?("<h2>Scan from camera</h2>")
    assert last_response.body.include?("<h2>Scan from file:</h2>")
  end

  def test_should_register_equipment
    datasheet = mock
    Rasp.stubs(:datasheet).returns(nil)
    app.any_instance.stubs(:valid_setup_code?).returns(true)
    app.any_instance.stubs(:build_datasheet).returns(datasheet)
    get "/register/V4L1D-C0D3"

    assert_response :created, last_response
  end

  def test_should_not_register_with_invalid_setup_code
    datasheet = mock()
    Rasp.stubs(:datasheet).returns(nil)
    get "/register/1234"

    assert_response :forbidden, last_response
  end

  def test_should_not_register_twice
    datasheet = mock()
    Rasp.stubs(:datasheet).returns(datasheet)
    get "/register/1234"

    assert_response :not_modified, last_response
  end

  def test_datasheet_download
    Rasp.stubs(:datasheet).returns(mock)
    app.any_instance.expects(:send_file).with(Rasp::DATASHEET_FILENAME, disposition: "attachment")
    get "/download/datasheet"
  end

  def test_should_not_donwload_datasheet_if_equipment_is_not_registered
    Rasp.stubs(:datasheet).returns(nil)
    get "/download/datasheet"

    assert_redirected_to("register", last_response)
  end

  def test_samples_download
    Rasp.stub_const(:SAVED_SAMPLES_PATH, "test/") do
      Rasp.stub_const(:ZIP_FILENAME, "test/samples.zip") do
        f1 = File.open(Rasp::SAVED_SAMPLES_PATH + "sample1.json", "w")
        f2 = File.open(Rasp::SAVED_SAMPLES_PATH + "sample2.json", "w")

        app.any_instance.expects(:send_file)
        get "/download/samples"

        assert file_exists?(Rasp::ZIP_FILENAME)

        files = [f1, f2, Rasp::ZIP_FILENAME]
        files.each do |file|
          delete_file(file)
        end
      end
    end
  end

  def test_wifi_setup
    get "/wifi_setup"

    assert(last_response.ok?)
    assert last_response.body.include?("<h1>Wi-Fi Setup</h1>")
    assert last_response.body.include?("<b>Network Name: </b>")
    assert last_response.body.include?("<b>Network Password: </b>")
  end

  def test_new_sample_creation
    app.any_instance.stubs(:new_sample).returns(200)
    get "/new_sample"
  end
end
