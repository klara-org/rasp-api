# frozen_string_literal: true

ENV["RACK_ENV"] = "test"
require "simplecov"

SimpleCov.start do
  add_filter "test/"
  add_filter "vendor/"
  add_filter "helpers/test_utils.rb"
end
SimpleCov.formatter = SimpleCov::Formatter::HTMLFormatter

require "minitest/autorun"
require "minitest/unit"
require "minitest/stub_const"
require "minitest/stub_any_instance"
require "mocha/minitest"
require "rack/test"
require "purdytest"
require_relative "../app"

class MainAppTest < Minitest::Test
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def assert_redirected_to(path, response)
    assert_match /#{path}/, response.header["Location"]
  end

  def assert_response(expected, response)
    msg = "Responses don't match"
    expected_status = Rack::Utils.status_code(expected)
    current_response = response.class == Integer ? response : response.status
    assert_equal(expected_status, current_response, msg)
  end
end
